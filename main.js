//Income
const incomeSalary = document.getElementById("income-salary");
incomeFreelance = document.getElementById("income-freelance");
incomeExtra1 = document.getElementById("income-extra-1");
incomeExtra2 = document.getElementById("income-extra-2");

// Costs
const costsFlat = document.getElementById("costs-flat");
costsHouseServices = document.getElementById("costs-house-services");
costsTransport = document.getElementById("costs-transport");
costCredit = document.getElementById("costs-credit");

// Total inputs
const totalMonthInput = document.getElementById("total-month");
totalDayInput = document.getElementById("total-day");
totalYearInput = document.getElementById("total-year");

let totalMonth, totalDay, totalYear;

//money box
const moneyBoxRange = document.getElementById("money-box-range");
accumulationIput = document.getElementById("accumulation");
spend = document.getElementById("spend");

let accumulation = 0;
let totalPercent = 0;

const inputs = document.querySelectorAll(".input");
for (input of inputs) {
  input.addEventListener("input", () => {
    countingAvailableMoney();
    calculationPersents();
  });
}

const strToNum = (str) => (str.value ? parseInt(str.value) : 0);

const countingAvailableMoney = () => {
  const totalPerMonth =
    strToNum(incomeSalary) +
    strToNum(incomeExtra1) +
    strToNum(incomeExtra2) +
    strToNum(incomeFreelance);
  const totalCosts =
    strToNum(costsFlat) +
    strToNum(costsHouseServices) +
    strToNum(costCredit) +
    strToNum(costsTransport);

  totalMonth = totalPerMonth - totalCosts;
  totalMonthInput.value = totalMonth;
};

moneyBoxRange.addEventListener("input", (e) => {
  const totalPerseltElement = document.getElementById("total-precents");
  totalPercent = e.target.value;
  totalPerseltElement.innerHTML = totalPercent;
  calculationPersents();
});

const calculationPersents = () => {
  if (isNaN(totalMonth)) {
    accumulation = 0;
  } else {
    accumulation = ((totalMonth * totalPercent) / 100).toFixed();
  }
  console.log(accumulation);
  accumulationIput.value = accumulation;

  spend.value = totalMonth - accumulation;
  totalDay = (spend.value / 30).toFixed();
  totalDayInput.value = totalDay;

  totalYear = accumulation * 12;
  totalYearInput.value = totalYear;
};
